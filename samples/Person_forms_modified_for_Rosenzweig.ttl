# SPDX-FileCopyrightText: 2023 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC-BY-4.0
#
# Person_forms_modified_for_Rosenzweig.ttl is the ROGER-schema which provides a reduced version of that in Person_forms_extended.ttl. It only includes features used in the Rosenzweig project.

@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>.
@prefix sh: <https://www.w3.org/ns/shacl#>.
@prefix dash: <http://datashapes.org/dash#>.
@prefix xsd: <http://www.w3.org/2001/XMLSchema#>.
@prefix ex: <http://example.com/ns#>.
@prefix exshapes: <http://beispiel.de/ns#>.
@prefix roger: <https://www.sub.uni-goettingen.de/roger/schema>.
@prefix gndo: <https://d-nb.info/standards/elementset/gnd#>.
@prefix exvoc: <http://beispiel.de/vocabulary#>.

exshapes:PersonShape
    a sh:NodeShape ;
    rdfs:label "Person Shape" ;
    sh:targetClass ex:Person ;
    roger:formNode true ;
    sh:xor (
          [sh:property exshapes:nameShape]
          [sh:property exshapes:GNDIDShape]
          ) ;
    sh:xor (
          [sh:property exshapes:aliveTrueShape ,
                       exshapes:hasAddressShape]
          [sh:property exshapes:aliveFalseShape]
          ) ;
    sh:property exshapes:maritalStatusShape ,
                exshapes:homepageShape ,
                exshapes:friendShape ,
                exshapes:commentShape .

exshapes:FriendShape
    a sh:NodeShape ;
    rdfs:label "Person Shape" ;
    sh:and (
        [sh:targetClass ex:Person]
        [sh:or (
          [sh:targetObjectsOf ex:friend]
          [sh:targetSubjectsOf ex:friend]
                )]
        ) ;
    roger:formNode false ;
    sh:xor (
          [sh:property exshapes:nameShape]
          [sh:property exshapes:GNDIDShape]
          ) ;
    sh:xor (
          [sh:property exshapes:aliveTrueShape ,
                       exshapes:hasAddressShape]
          [sh:property exshapes:aliveFalseShape]
          ) ;
    sh:property exshapes:maritalStatusShape ,
                exshapes:homepageShape ,
                exshapes:friendShape ,
                exshapes:commentShape .

exshapes:nameShape
    a sh:PropertyShape ;
    sh:path ex:name ;
    sh:datatype xsd:string ;
    sh:minCount 1 ;
    sh:maxCount 1 ;
    dash:editor dash:TextFieldEditor ;
    dash:singleLine true ;
    sh:name "Name" ;
    sh:description "Enter the name" ;
    sh:order "1"^^xsd:decimal .

exshapes:gndIDShape
    a sh:PropertyShape ;
    sh:path ex:gndID ;
    sh:minCount 1 ;
    dash:editor [
        a roger:SelectInstanceFromRemoteDatasetEditor ;
        roger:searchQueryURL "https://portal.dnb.de/opac.atom?currentResultId=per%3D%22{searchTerms}%22%26any%26persons&amp;method=search&amp" ;
        roger:searchQueryType "application/atom+xml"
        ] ;
    sh:node exshapes:GNDIDShape ;
    sh:nodeKind sh:IRI ;
    sh:description "Select an entry from the GND (Gemeinsame Normdatei) of the German National Library." ;
    sh:name "GND" ;
    sh:order "2"^^xsd:decimal .

exshapes:GNDIDShape
    a sh:NodeShape ;
    rdfs:label "GND-ID Shape" ;
    sh:targetClass ex:GNDID ;
    roger:formNode false .

exshapes:aliveTrueShape
    a sh:PropertyShape ;
    sh:path ex:alive ;
    dash:editor dash:BooleanSelectEditor ;
    sh:datatype xsd:boolean ;
    sh:in true ;
    sh:name "Alive" ;
    sh:minCount 1 ;
    sh:maxCount 1 ;
    sh:description "True if the person is still alive" ;
    sh:order "3"^^xsd:decimal .

exshapes:aliveFalseShape
    a sh:PropertyShape ;
    sh:path ex:alive ;
    dash:editor dash:BooleanSelectEditor ;
    sh:datatype xsd:boolean ;
    sh:in false ;
    sh:name "Alive" ;
    sh:minCount 1 ;
    sh:maxCount 1 ;
    sh:description "True if the person is still alive" ;
    sh:order "3"^^xsd:decimal .

exshapes:maritalStatusShape
    a sh:PropertyShape ;
    sh:path ex:maritalStatus ;
    sh:class exvoc:MaritalStatus ;
    sh:description "Choose the marital status" ;
    sh:defaultValue exvoc:single ;
    sh:minCount 1 ;
    sh:maxCount 1 ;
    dash:editor dash:AutoCompleteEditor ;
    sh:node exshapes:MaritalStatusShape;
    sh:nodeKind sh:IRI ;
    sh:name "Marital Status" ;
    sh:order "4"^^xsd:decimal .

exshapes:homepageShape
    a sh:PropertyShape ;
    sh:path ex:homepage ;
    dash:editor dash:URIEditor ;
    sh:nodeKind sh:Literal ;
    sh:datatype xsd:anyURI ;
    sh:name "Homepage" ;
    sh:description "Enter a URL" ;
    sh:order "5"^^xsd:decimal .

exshapes:hasAddressShape
    a sh:PropertyShape ;
    sh:path ex:hasAddress ;
    dash:editor dash:DetailsEditor ;
    sh:node exshapes:AddressShape ;
    sh:nodeKind sh:BlankNode ;
    sh:name "Has Address" ;
    sh:order "6"^^xsd:decimal .

exshapes:AddressShape
    a sh:NodeShape ;
    rdfs:label "Address Shape" ;
    sh:targetClass ex:Address ;
    roger:formNode false ;
    sh:and (
      [sh:property exshapes:zipCodeShape]
      [sh:property exshapes:streetHouseNumberShape]
      ) .

exshapes:zipCodeShape
    a sh:PropertyShape ;
    sh:path ex:zipCode ;
    sh:datatype xsd:string ;
    dash:editor dash:TextFieldEditor ;
    dash:singleLine true ;
    sh:minCount 1 ;
    sh:maxCount 1 ;
    sh:name "ZIP Code" ;
    sh:pattern "[0-9]{5}" ;
    sh:message "The ZIP code has to consist of 5 numbers." ;
    sh:description "Enter the ZIP Code" ;
    sh:order "1"^^xsd:decimal .

exshapes:streetHouseNumberShape
    a sh:PropertyShape ;
    sh:path ex:streetHouseNumber ;
    sh:datatype xsd:string ;
    dash:editor dash:TextFieldEditor ;
    dash:singleLine true ;
    sh:minCount 1 ;
    sh:maxCount 1 ;
    sh:name "Street Name and House Number" ;
    sh:description "Enter the Street Name and House Number" ;
    sh:order "2"^^xsd:decimal .

exshapes:friendShape
    a sh:PropertyShape ;
    sh:path ex:friend ;
    sh:name "Friend" ;
    sh:class ex:Person ;
    dash:editor dash:AutoCompleteEditor ;
    dash:singleLine true ;
    roger:searchIn ex:name ,
                   ex:GNDID ,
                   ( ex:hasAddress [ex:Address ex:streetHouseNumber] ) ;
    sh:node exshapes:PersonShape ;
    sh:nodeKind sh:IRI ;
    sh:description "Select another person as a friend" ;
    sh:order "7"^^xsd:decimal .

exshapes:commentShape
    a sh:PropertyShape ;
    sh:path ex:comment ;
    dash:editor dash:TextAreaEditor ;
    sh:datatype xsd:string ;
    sh:name "Comment" ;
    sh:maxCount 1 ;
    sh:description "Enter a comment" ;
    sh:order "8"^^xsd:decimal .

exshapes:AddressBookShape
    a sh:NodeShape ;
    rdfs:label "Address Book Shape" ;
    sh:targetClass ex:AddressBook ;
    roger:formNode true ;
    sh:property exshapes:includesShape .

exshapes:includesShape
    a sh:PropertyShape ;
    sh:path ex:includes ;
    sh:name "Includes" ;
    sh:class ex:Person ;
    dash:editor dash:AutoCompleteEditor ;
    roger:searchIn ex:name ,
    # or SPARQL?
                   ex:gndID ,
                   ( ex:hasAddress [ex:Address ex:streetHouseNumber] ) ;
    dash:singleLine true ;
    sh:node exshapes:FriendShape ;
    sh:nodeKind sh:IRI ;
    sh:order "1"^^xsd:decimal .