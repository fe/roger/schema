<!--
SPDX-FileCopyrightText: 2022 Georg-August-Universität Göttingen

SPDX-License-Identifier: CC0-1.0
-->

# ROGER-Schema

## License

This project aims to be [REUSE compliant](https://reuse.software/spec/).
Original parts are licensed under CC-BY-4.0.
Other assets are licensed under the respective license of the original.
Documentation, configuration and generated files are licensed under CC0-1.0.

## Badges

[![REUSE status](https://api.reuse.software/badge/gitlab.gwdg.de/fe/roger/schema)](https://api.reuse.software/info/gitlab.gwdg.de/fe/roger/schema)
